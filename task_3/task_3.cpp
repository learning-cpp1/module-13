#include <iostream>
#include <vector>

int main(){
    std::vector <int> vec {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
    int num = 0, co = 0;

    while (true)
    {
         while (num != -1)
        {
            std::cout << "Input number: ";
            std::cin >> num;
            if(num == -1) 
                break;
            if(co < 20) {
                vec[co] = num;
                co++;
            } else {
                vec.erase(vec.begin());
                vec.push_back(num);
            }
        }
        std::cout << "output: ";
        for (int i = 0; i < vec.size(); i++)
            std::cout << vec[i] << " ";
        std::cout << "\n";
        num = 0;
    }
}