#include <iostream>
#include <vector>


std::vector<int> ascentDelete(std::vector<int>v, int d){ // функция перемещения удаляемого в конец вектора
    for(int i = 0; i < v.size(); i++){
        if (v[i] == d) {
            for (int j = i; j < v.size()-1; j++)
                v[j]=v[j+1]; 
            v[v.size()-1] = d;
            break;
        }    
    }
    return v;
}

int main(){
    int n;
    std::cout << "Input vector size: ";
    std::cin >> n;
    std::vector <int> vec(n);
    std::cout << "Input numbers: ";
    for (int i = 0; i < n; i++)
        std::cin >> vec[i];
    std::cout << "Input number to delete: ";
    int del, co = 0;
    std::cin >> del;
    for (int i = 0; i < vec.size(); i++)// подсчет количества удаляемых элементо
        if(vec[i] == del) co++;
    for (int i =0; i < co; i++){// 横цикл удаления перемещенных в конец вектора элементов
        vec = ascentDelete(vec, del);
        vec.pop_back();
    }
    std::cout << "Result: ";
    for (int i = 0; i < vec.size(); i++)
        std::cout << vec[i]<< " ";

}